package run;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author zzl
 * @date 2024/1/18 10:02
 */
public class Test1 {
    public static void main(String[] args) {
        int[] intArr = new int[]{1,2,3};
        ext(intArr);
        for (int i : intArr) {
            System.out.println(i);
        }
    }
    public static void ext(int[] arr){
        arr[0] = 10000;
        arr[2] = 10000;
    }
}
