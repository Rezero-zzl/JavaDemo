package project.clone;

/**
 * clone方法的深浅拷贝
 * @author zzl
 * @date 2024/1/18 18:06
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        A a = new A(new B("qq"));
        A cloneA = (A)a.clone();
        System.out.println(a.getB().getB() + ":" + cloneA.getB().getB());
        cloneA.getB().setB("ww");
        System.out.println(a.getB().getB() + ":" + cloneA.getB().getB());
    }
}
