package project.clone;

/**
 * @author zzl
 * @date 2024/1/18 13:59
 */
public class A implements Cloneable {
    private B b;  // 假如a是自定义的对象类型，没有实现clone方法，则需要注意浅拷贝问题

    public A(B b) {
        this.b = b;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        A temp = (A)super.clone();
        temp.setB((B)temp.getB().clone());
        return temp;
    }
}

