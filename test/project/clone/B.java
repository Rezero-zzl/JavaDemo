package project.clone;

/**
 * @author zzl
 * @date 2024/1/18 16:07
 */
public class B implements Cloneable{
    private String b;

    public B(){}
    public B(String b) {
        this.b = b;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
