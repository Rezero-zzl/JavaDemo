package project.serialize;

import java.io.Serializable;

/**
 * @author zzl
 * @date 2024/1/18 17:35
 */
public class Person implements Serializable {
    public static int code = 1;
    private static final long serialVersionUID = 123456789L;
    private String name;
    private Integer age;

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
