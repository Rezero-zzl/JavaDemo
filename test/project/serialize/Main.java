package project.serialize;

import java.io.*;

/**
 * 序列化不保存静态变量
 * @author zzl
 * @date 2024/1/18 17:37
 */
public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("张三",8);
        Person person2 = new Person("李四",9);
        System.out.println("person1序列化前的静态变量：" + person1.code);
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("temp.txt"))){
            outputStream.writeObject(person1);
            System.out.println("person1序列化完成");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        person2.code = 2;
        System.out.println("通过person2改变Person类的静态变量为2");
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("temp.txt"))){
            Person person = (Person) inputStream.readObject();
            System.out.println("反序列化person1后取其静态变量："+person.code);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
